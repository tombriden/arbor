Upstream: yes, in 4.15
Reason: linux-headers does not properly support musl libc.

From d0367ece54eb4d49e97403cee90b6f95a6bfc75f Mon Sep 17 00:00:00 2001
From: Kylie McClain <somasis@exherbo.org>
Date: Sun, 22 May 2016 21:57:53 -0400
Subject: [PATCH] musl fixes

---
 include/uapi/linux/if_ether.h    |  4 +++-
 include/uapi/linux/kernel.h      |  2 --
 include/uapi/linux/libc-compat.h | 42 ++++++++++++++++++++++------------------
 include/uapi/linux/tcp.h         |  3 +++
 4 files changed, 29 insertions(+), 22 deletions(-)

diff --git a/include/uapi/linux/if_ether.h b/include/uapi/linux/if_ether.h
index cec849a..b490367 100644
--- a/include/uapi/linux/if_ether.h
+++ b/include/uapi/linux/if_ether.h
@@ -22,6 +22,7 @@
 #define _UAPI_LINUX_IF_ETHER_H
 
 #include <linux/types.h>
+#include <linux/libc-compat.h>
 
 /*
  *	IEEE 802.3 Ethernet magic constants.  The frame sizes omit the preamble
@@ -137,11 +138,12 @@
  *	This is an Ethernet frame header.
  */
 
+#if __UAPI_DEF_ETHHDR
 struct ethhdr {
 	unsigned char	h_dest[ETH_ALEN];	/* destination eth addr	*/
 	unsigned char	h_source[ETH_ALEN];	/* source ether addr	*/
 	__be16		h_proto;		/* packet type ID field	*/
 } __attribute__((packed));
-
+#endif
 
 #endif /* _UAPI_LINUX_IF_ETHER_H */
diff --git a/include/uapi/linux/kernel.h b/include/uapi/linux/kernel.h
index 466073f..30d07b7 100644
--- a/include/uapi/linux/kernel.h
+++ b/include/uapi/linux/kernel.h
@@ -1,8 +1,6 @@
 #ifndef _UAPI_LINUX_KERNEL_H
 #define _UAPI_LINUX_KERNEL_H
 
-#include <linux/sysinfo.h>
-
 /*
  * 'kernel.h' contains some often-used function prototypes etc
  */
diff --git a/include/uapi/linux/libc-compat.h b/include/uapi/linux/libc-compat.h
index e4f048e..19efa6c 100644
--- a/include/uapi/linux/libc-compat.h
+++ b/include/uapi/linux/libc-compat.h
@@ -48,13 +48,13 @@
 #ifndef _UAPI_LIBC_COMPAT_H
 #define _UAPI_LIBC_COMPAT_H
 
-/* We have included glibc headers... */
-#if defined(__GLIBC__)
+/* We are used from userspace... */
+#if !defined(__KERNEL__)
 
-/* Coordinate with glibc net/if.h header. */
-#if defined(_NET_IF_H) && defined(__USE_MISC)
+/* Coordinate with libc net/if.h header. */
+#if defined(_NET_IF_H)
 
-/* GLIBC headers included first so don't define anything
+/* libc headers included first so don't define anything
  * that would already be defined. */
 
 #define __UAPI_DEF_IF_IFCONF 0
@@ -85,10 +85,22 @@
 
 #endif /* _NET_IF_H */
 
+#ifdef _NETINET_IF_ETHER_H /* musl */
+#define __UAPI_DEF_ETHHDR 0
+#else /* glibc uses __NETINET_IF_ETHER_H, and includes the kernel header. */
+#define __UAPI_DEF_ETHHDR 1
+#endif
+
+#ifdef _NETINET_TCP_H /* musl */
+#define __UAPI_DEF_TCPHDR 0
+#else
+#define __UAPI_DEF_TCPHDR 1
+#endif
+
 /* Coordinate with glibc netinet/in.h header. */
 #if defined(_NETINET_IN_H)
 
-/* GLIBC headers included first so don't define anything
+/* libc headers included first so don't define anything
  * that would already be defined. */
 #define __UAPI_DEF_IN_ADDR		0
 #define __UAPI_DEF_IN_IPPROTO		0
@@ -98,15 +110,7 @@
 #define __UAPI_DEF_IN_CLASS		0
 
 #define __UAPI_DEF_IN6_ADDR		0
-/* The exception is the in6_addr macros which must be defined
- * if the glibc code didn't define them. This guard matches
- * the guard in glibc/inet/netinet/in.h which defines the
- * additional in6_addr macros e.g. s6_addr16, and s6_addr32. */
-#if defined(__USE_MISC) || defined (__USE_GNU)
 #define __UAPI_DEF_IN6_ADDR_ALT		0
-#else
-#define __UAPI_DEF_IN6_ADDR_ALT		1
-#endif
 #define __UAPI_DEF_SOCKADDR_IN6		0
 #define __UAPI_DEF_IPV6_MREQ		0
 #define __UAPI_DEF_IPPROTO_V6		0
@@ -114,10 +118,10 @@
 #define __UAPI_DEF_IN6_PKTINFO		0
 #define __UAPI_DEF_IP6_MTUINFO		0
 
-#else
+#else /* defined(_NETINET_IN_H) */
 
 /* Linux headers included first, and we must define everything
- * we need. The expectation is that glibc will check the
+ * we need. The expectation is that the libc will check the
  * __UAPI_DEF_* defines and adjust appropriately. */
 #define __UAPI_DEF_IN_ADDR		1
 #define __UAPI_DEF_IN_IPPROTO		1
@@ -127,7 +131,7 @@
 #define __UAPI_DEF_IN_CLASS		1
 
 #define __UAPI_DEF_IN6_ADDR		1
-/* We unconditionally define the in6_addr macros and glibc must
+/* We unconditionally define the in6_addr macros and libc must
  * coordinate. */
 #define __UAPI_DEF_IN6_ADDR_ALT		1
 #define __UAPI_DEF_SOCKADDR_IN6		1
@@ -149,7 +153,7 @@
 /* If we did not see any headers from any supported C libraries,
  * or we are being included in the kernel, then define everything
  * that we need. */
-#else /* !defined(__GLIBC__) */
+#else /* !defined(__KERNEL__) */
 
 /* Definitions for if.h */
 #define __UAPI_DEF_IF_IFCONF 1
@@ -182,6 +186,6 @@
 /* Definitions for xattr.h */
 #define __UAPI_DEF_XATTR		1
 
-#endif /* __GLIBC__ */
+#endif /* defined(__KERNEL__) */
 
 #endif /* _UAPI_LIBC_COMPAT_H */
diff --git a/include/uapi/linux/tcp.h b/include/uapi/linux/tcp.h
index 53e8e3f..da10331 100644
--- a/include/uapi/linux/tcp.h
+++ b/include/uapi/linux/tcp.h
@@ -18,9 +18,11 @@
 #define _UAPI_LINUX_TCP_H
 
 #include <linux/types.h>
+#include <linux/libc-compat.h>
 #include <asm/byteorder.h>
 #include <linux/socket.h>
 
+#if __UAPI_DEF_TCPHDR
 struct tcphdr {
 	__be16	source;
 	__be16	dest;
@@ -55,6 +57,7 @@ struct tcphdr {
 	__sum16	check;
 	__be16	urg_ptr;
 };
+#endif
 
 /*
  *	The union cast uses a gcc extension to avoid aliasing problems
-- 
2.9.0

Date: Thu, 10 Nov 2016 20:40:44 -0500
From: Felix Janda <felix.janda@...teo.de>
To: linux-devel@...r.kernel.org
Cc: "David S. Miller" <davem@...emloft.net>, linux-api@...r.kernel.org,
	musl@...ts.openwall.com
Subject: [PATCH] uapi libc compat: allow non-glibc to opt out of uapi
 definitions

Currently, libc-compat.h detects inclusion of specific glibc headers,
and defines corresponding _UAPI_DEF_* macros, which in turn are used in
uapi headers to prevent definition of conflicting structures/constants.
There is no such detection for other c libraries, for them the
_UAPI_DEF_* macros are always defined as 1, and so none of the possibly
conflicting definitions are suppressed.

This patch enables non-glibc c libraries to request the suppression of
any specific interface by defining the corresponding _UAPI_DEF_* macro
as 0.

This patch together with the recent musl libc commit

http://git.musl-libc.org/cgit/musl/commit/?id=04983f2272382af92eb8f8838964ff944fbb8258

fixes the following compiler errors when <linux/in6.h> is included
after musl <netinet/in.h>:

./linux/in6.h:32:8: error: redefinition of 'struct in6_addr'
./linux/in6.h:49:8: error: redefinition of 'struct sockaddr_in6'
./linux/in6.h:59:8: error: redefinition of 'struct ipv6_mreq'

Signed-off-by: Felix Janda <felix.janda@...teo.de>
---
 include/uapi/linux/libc-compat.h | 52 ++++++++++++++++++++++++++++++++++++++++
 1 file changed, 52 insertions(+)

diff --git a/include/uapi/linux/libc-compat.h b/include/uapi/linux/libc-compat.h
index 44b8a6b..c316725 100644
--- a/include/uapi/linux/libc-compat.h
+++ b/include/uapi/linux/libc-compat.h
@@ -171,42 +171,94 @@
 #else /* !defined(__GLIBC__) */
 
 /* Definitions for if.h */
+#if !defined(__UAPI_DEF_IF_IFCONF)
 #define __UAPI_DEF_IF_IFCONF 1
+#endif
+#if !defined(__UAPI_DEF_IF_IFMAP)
 #define __UAPI_DEF_IF_IFMAP 1
+#endif
+#if !defined(__UAPI_DEF_IFNAMSIZ)
 #define __UAPI_DEF_IF_IFNAMSIZ 1
+#endif
+#if !defined(__UAPI_DEF_IFREQ)
 #define __UAPI_DEF_IF_IFREQ 1
+#endif
 /* Everything up to IFF_DYNAMIC, matches net/if.h until glibc 2.23 */
+#if !defined(__UAPI_DEF_IF_NET_DEVICE_FLAGS)
 #define __UAPI_DEF_IF_NET_DEVICE_FLAGS 1
+#endif
 /* For the future if glibc adds IFF_LOWER_UP, IFF_DORMANT and IFF_ECHO */
+#if !defined(__UAPI_DEF_IF_NET_DEVICE_FLAGS_LOWER_UP_DORMANT_ECHO)
 #define __UAPI_DEF_IF_NET_DEVICE_FLAGS_LOWER_UP_DORMANT_ECHO 1
+#endif
 
 /* Definitions for in.h */
+#if !defined(__UAPI_DEF_IN_ADDR)
 #define __UAPI_DEF_IN_ADDR		1
+#endif
+#if !defined(__UAPI_DEF_IN_IPPROTO)
 #define __UAPI_DEF_IN_IPPROTO		1
+#endif
+#if !defined(__UAPI_DEF_IN_PKTINFO)
 #define __UAPI_DEF_IN_PKTINFO		1
+#endif
+#if !defined(__UAPI_DEF_IP_MREQ)
 #define __UAPI_DEF_IP_MREQ		1
+#endif
+#if !defined(__UAPI_DEF_SOCKADDR_IN)
 #define __UAPI_DEF_SOCKADDR_IN		1
+#endif
+#if !defined(__UAPI_DEF_IN_CLASS)
 #define __UAPI_DEF_IN_CLASS		1
+#endif
 
 /* Definitions for in6.h */
+#if !defined(__UAPI_DEF_IN6_ADDR)
 #define __UAPI_DEF_IN6_ADDR		1
+#endif
+#if !defined(__UAPI_DEF_IN6_ADDR_ALT)
 #define __UAPI_DEF_IN6_ADDR_ALT		1
+#endif
+#if !defined(__UAPI_DEF_SOCKADDR_IN6)
 #define __UAPI_DEF_SOCKADDR_IN6		1
+#endif
+#if !defined(__UAPI_DEF_IPV6_MREQ)
 #define __UAPI_DEF_IPV6_MREQ		1
+#endif
+#if !defined(__UAPI_DEF_IPPROTO_V6)
 #define __UAPI_DEF_IPPROTO_V6		1
+#endif
+#if !defined(__UAPI_DEF_IPV6_OPTIONS)
 #define __UAPI_DEF_IPV6_OPTIONS		1
+#endif
+#if !defined(__UAPI_DEF_IN6_PKTINFO)
 #define __UAPI_DEF_IN6_PKTINFO		1
+#endif
+#if !defined(__UAPI_DEF_IP6_MTUINFO)
 #define __UAPI_DEF_IP6_MTUINFO		1
+#endif
 
 /* Definitions for ipx.h */
+#if !defined(__UAPI_DEF_SOCKADDR_IPX)
 #define __UAPI_DEF_SOCKADDR_IPX			1
+#endif
+#if !defined(__UAPI_DEF_IPX_ROUTE_DEFINITION)
 #define __UAPI_DEF_IPX_ROUTE_DEFINITION		1
+#endif
+#if !defined(__UAPI_DEF_IPX_INTERFACE_DEFINITION)
 #define __UAPI_DEF_IPX_INTERFACE_DEFINITION	1
+#endif
+#if !defined(__UAPI_DEF_IPX_CONFIG_DATA)
 #define __UAPI_DEF_IPX_CONFIG_DATA		1
+#endif
+#if !defined(__UAPI_DEF_IPX_ROUTE_DEF)
 #define __UAPI_DEF_IPX_ROUTE_DEF		1
+#endif
 
 /* Definitions for xattr.h */
+#if !defined(__UAPI_DEF_XATTR)
 #define __UAPI_DEF_XATTR		1
+#endif
 
 #endif /* __GLIBC__ */
 
-- 
2.7.3
